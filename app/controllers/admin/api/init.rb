# frozen_string_literal: true

module Admin
  module Api
    class Init < Grape::API
      insert_after Grape::Middleware::Formatter, Grape::Middleware::Logger,
          logger: Logger.new(STDERR),
          filter: Class.new { def filter(opts) opts.reject { |k, _| k.to_s == "password" } end }.new,
          headers: %w(version cache-control)

      # Build params using object
      include Grape::Extensions::Hashie::Mash::ParamBuilder

      # make phone standar format indonesia
      # before do
      #   params.phone = Utility.phone_converter(params.phone) if params.phone.present?
      # end

      mount Admin::Api::V1::Main

      # swagger settings
      # versions = ["v1", "v2"]
      # options = {}

      # GrapeSwaggerRails.options.before_action do |request|
      #   x = URI.parse(request.url).request_uri.to_s.split("/")[1]

      #   versions.each do |v|
      #     if URI.parse(request.url).request_uri.eql?("/#{x}/doc/#{v}/")
      #       options = { version: "#{v}" }
      #       break
      #     end
      #   end
      #   GrapeSwaggerRails.options.app_url            = "/#{x}/api/#{options[:version]}/documentation"
      #   GrapeSwaggerRails.options.url                = ".json"
      #   GrapeSwaggerRails.options.hide_url_input     = true
      #   GrapeSwaggerRails.options.hide_api_key_input = true
      #   GrapeSwaggerRails.options.api_auth     = "bearer"
      #   GrapeSwaggerRails.options.api_key_name = "Authorization"
      #   GrapeSwaggerRails.options.api_key_type = "header"
      # end
    end
  end
end
