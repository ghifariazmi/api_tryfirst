class Admin::Api::V1::Books::Resources::Books < Grape::API
    resource :books do
        desc "Get List Books"
        oauth2 "super_admin"
        params do
            optional :q, type: String, desc: "Your key search"
        end

        get "/" do
            key = params.q
            p key
            unless key.present?
                data = Book.all
            else
                data = Book.where("name ILIKE ?", params.q)
            end
            present :books, data, with: Admin::Api::V1::Users::Entities::Books
        end

        desc "Save Books"
        oauth2 "super_admin"
        params do
            requires :name, type: String
            requires :cover_book,type: File
            requires :user_id
        end
        post "/" do
            user = User.find(params[:user_id])
            error!("User not found", env["api.response.code"] = 422) unless user.present?

            book = Book.new
            book.name = params.name
            book.cover_book = params.cover_book
            book.user_id = user.id
            error!(book.errors) unless book.save
            env["api.response.message"] = "Book saved"
            present :books, book, with: Admin::Api::V1::Users::Entities::Books
        end
    end
end