# frozen_string_literal: true
require_relative '../../../config.rb'
require_relative '../../../exception_handlers.rb'
module Admin
  module Api
    module V1
      class Main < Grape::API
        include Config

        prefix "api"
        version "v1", using: :path

        # Exception Handlers
        include ExceptionHandlers unless ENV["DEBUGGING"].eql?("true") && Rails.env.development?

        use ::WineBouncer::OAuth2

        helpers do
          def current_user
            resource_owner
          end
        end

        mount Admin::Api::V1::Users::Routes
        mount Admin::Api::V1::Oauth::Routes
        mount Admin::Api::V1::Books::Routes

      end
    end
  end
end
