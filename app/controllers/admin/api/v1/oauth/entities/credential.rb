# frozen_string_literal: true

class Admin::Api::V1::Oauth::Entities::Credential < Grape::Entity
  expose :token, as: :access_token
  expose :refresh_token
  expose :token_type
  expose :expires_in
end
