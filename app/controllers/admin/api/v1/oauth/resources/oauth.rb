# frozen_string_literal: true

class Admin::Api::V1::Oauth::Resources::Oauth < Grape::API
  resource "oauth" do
    desc "Login as user"
    params do
      requires :email, type: String
      requires :password, type: String
    end
    post "/" do
      p "verifikasi sampai"
      user = ::User.find_by(email: params.email).try(:authenticate, params.password)
      error!("Invalid Login", 401) unless user.present?

      app = Doorkeeper::Application.find_by_name("Pos Mage Dashboard")
      Doorkeeper::AccessToken.where(resource_owner_id: user.id, application: app, revoked_at: nil).each(&:revoke)
      token = Doorkeeper::AccessToken.create!(
        application: app,
          expires_in: Doorkeeper.configuration.access_token_expires_in.to_i,
          resource_owner_id: user.id,
          use_refresh_token: Doorkeeper.configuration.refresh_token_enabled?
      )

      present :profile, user #, with: Admin::Api::V1::User::Entities::User
      present :access_tokens, token, with: Admin::Api::V1::Oauth::Entities::Credential
    end

    desc "Refresh token"
    params do
      requires :token, type: String
    end
    post :refresh_token do
      token = Doorkeeper::AccessToken.by_refresh_token(params.token)
      present :access_tokens, token, with: Admin::Api::V1::Oauth::Entities::Credential
    end
  end
end
