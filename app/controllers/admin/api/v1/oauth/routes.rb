# frozen_string_literal: true
require_relative '../../../../error_formatter.rb'
require_relative '../../../../success_formatter.rb'
module Admin
  module Api
    module V1
      module Oauth
        class Routes < Grape::API
          formatter :json, SuccessFormatter
          error_formatter :json, ErrorFormatter

          mount Admin::Api::V1::Oauth::Resources::Oauth
        end
      end
    end
  end
end
