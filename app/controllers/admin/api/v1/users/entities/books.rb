class Admin::Api::V1::Users::Entities::Books < Grape::Entity
    expose :id
    expose :name
    expose :url_cover, as: :cover_book
    expose :user_id

    private
    def url_cover
        object.try(:cover_book).try(:url)
    end
end