class Admin::Api::V1::Users::Entities::Users < Grape::Entity
    expose :id
    expose :email
    expose :name
    expose :phone
    expose :roles, using: Admin::Api::V1::Users::Entities::Roles
    expose :books, as: :books, using: Admin::Api::V1::Users::Entities::Books
    # expose :roles, as: :roles, using: Api::V1::Users::Entities::Roles
end