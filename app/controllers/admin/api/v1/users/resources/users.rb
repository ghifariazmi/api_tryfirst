class Admin::Api::V1::Users::Resources::Users < Grape::API
    resource :users do
        desc "Get List Users"
        oauth2 "super_admin"
        params do
            requires :q, type: String
        end

        get "/" do
            key = params.q
            unless key.present?
                data = User.all
            else
                data = User.where("name ILIKE ?", params.q)
            end
            present :users, data, with: Admin::Api::V1::Users::Entities::Users
        end

        desc "Create User"
        oauth2 "super_admin"
        params do
            requires :email, type: String
            requires :name, type: String
            requires :password, type: String
            requires :password_confirmation, type: String
            requires :phone, type: String
            requires :role_id, type: String
        end
        post "/" do
            role = Role.find_by_id(params.role_id)
            user = ::User.new
            user.email = params.email
            user.name = params.name
            user.password = params.password
            user.password_confirmation = params.password_confirmation
            user.phone = params.phone
            user.roles << role
            error!(user.errors.full_messages.join(", "), 422) unless  user.save

            env["api.response.message"] = "User Has Been Created"
            present :users, user, with: Admin::Api::V1::Users::Entities::Users
        end

        desc "Show User"
        oauth2 "super_admin"
        get "/:id" do
            data = User.find_by_id(params.id)
            error!("User not found", env["api.response.code"] = 422) unless data.present?
            present :users, data, with: Admin::Api::V1::Users::Entities::Users
        end

        desc "Delete User"
        oauth2 "super_admin"
        delete "/delete/:id" do
            data = User.find_by_id(params.id)
            error!("User not found", env["api.response.code"] = 422) unless data.present?

            User.find(params[:id]).destroy!
            #present :users, data, with: Api::V1::Users::Entities::Users
            env["api.response.message"] = "User Deleted"
            present :users, nil
        end

        desc "Update User"
        oauth2 "super_admin"
        params do
            requires :email, type: String
            requires :name, type: String
            optional :password, type: String
            optional :password_confirmation, type: String
            optional :phone, type: String
        end
        post "/update/:id" do
            user = User.find(params[:id])
            error!("User not found", env["api.response.code"] = 422) unless user.present?
            user.update(declared(params))
            error!(user.errors) unless user.save
            #SaveBookJob.perform_later(params[:id],params.username)

            env["api.response.message"] = "User Created"
            present :users, user, with: Admin::Api::V1::Users::Entities::Users
        end

        desc "Update User Job"
        oauth2 "super_admin"
        params do
            requires :email, type: String
            requires :name, type: String
            optional :password, type: String
            optional :password_confirmation, type: String
            optional :phone, type: String
        end
        post "/update-job/:id" do
            user = User.find(params[:id])
            error!("User not found", env["api.response.code"] = 422) unless user.present?

            inputs = declared(params)
            SaveUserJob.perform_later(inputs,user.id)

            env["api.response.message"] = "User Created"
            present :users, nil
        end
    end
end