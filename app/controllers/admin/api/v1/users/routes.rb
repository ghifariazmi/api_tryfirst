require_relative '../../../../error_formatter.rb'
require_relative '../../../../success_formatter.rb'
module Admin
  module Api
    module V1
      module Users
        class Routes < Grape::API
          formatter :json, SuccessFormatter
          error_formatter :json, ErrorFormatter

          mount Admin::Api::V1::Users::Resources::Users
        end
      end
    end
  end
end
