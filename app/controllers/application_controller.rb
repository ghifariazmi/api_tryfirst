class ApplicationController < ActionController::API
    rescue_from ActionController::RoutingError do |exception|
        render json: { error: { errors: ["Oh uh Oh you'are lost ??"] } }, status: 404
      end
    
    def catch_404
        raise ActionController::RoutingError.new(params[:path])
    end
    # before_action :require_user

    # helper_method :current_user, :logged_in?

    # def current_user
    #     @current_user ||= User.find(session[:user_id]) if session[:user_id]
    # end

    # def logged_in?
    #     !!current_user
    # end

    # def require_user
    #     if !logged_in?
    #         flash[:warning] = "Your must be logged in to perform this action."
    #         redirect_to login_path
    #     end
    # end
end
