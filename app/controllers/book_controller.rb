class BookController < ApplicationWebController
    def index
        @books = Book.all
        respond_to do |format|
            format.xlsx
            format.html
        end
    end

    def create
        p params.permit(:book_id, :user_id, :total_day)
        # @current_user = current_user

        # @product = Book.new()
        # @product.name = params.name
        # # @product = @current_user.products.new(product_params)

        # if @product.save
        #     redirect_to @product
        #   else
        #     render 'create'
        # end
        @books = Book.pluck(:name,:id)
        @bookData = @books.to_a
        @users = User.pluck(:name,:id)
        @datas = @users.to_a

        render 'create'
    end

    def store
    
    end

    def exportpdf
        @books = Book.all
        respond_to do |format|
            format.html
            format.pdf do
                pdf = BookPdf.new(@books)
                send_data pdf.render, filename: "book.pdf",
                                        type: "application/pdf",
                                        disposition: "inline"
            end
        end
    end

    private
    def product_params
        params.require(:product).permit(:product_name, :category, :description, :price, :contact_email, :cover, :term)
    end
end