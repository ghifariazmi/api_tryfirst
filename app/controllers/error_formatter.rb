# frozen_string_literal: true

module ErrorFormatter
  def self.call(message, backtrace, options, env, original_exception)
    {
        error:    {
            code: env["api.response.code"],
            errors: [message]
        }
    }.to_json
  end
end
