# frozen_string_literal: true

module ExceptionHandlers
  def self.included(base)
    base.instance_eval do
      rescue_from :all do |e|
        if e.class.name == "Grape::Exceptions::ValidationErrors"
          code    = 422
          message = e.as_json_custom
        elsif e.class.name == "RuntimeError" && e.message == "Invalid base64 string"
          code    = 406
          message = ["401 Unauthorized"]
        elsif e.class.name == ActiveRecord::RecordNotFound
          exceptions_handler(e.message, 404)
          code    = 404
          message = [e.message]
        elsif e.class.name == "WineBouncer::Errors::OAuthForbiddenError"
          code    = 403
          message = ["Access Forbidden"]
        elsif e.class.name == "WineBouncer::Errors::OAuthUnauthorizedError"
          code    = 401
          message = ["Invalid Access token"]
        else
          code    = 500
          message = [e.message]
        end
        results = {
            error: {
                code:   code,
                errors: message
            }
        }
        rack_response (results).to_json, code
      end
    end
  end
end
