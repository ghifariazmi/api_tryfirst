class SaveBookJob < ApplicationJob
    queue_as :default

    def perform(user_id,name,cover_book)
        book = Book.new
        book.name = name
        book.cover_book = cover_book
        book.user_id = user_id
        book.save
    end
end