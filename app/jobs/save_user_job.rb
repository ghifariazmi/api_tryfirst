class SaveUserJob < ApplicationJob
    queue_as :default

    def perform(params,user_id)
        user = User.find_by_id(user_id)
        user.update(params)
        user.save
    end
end