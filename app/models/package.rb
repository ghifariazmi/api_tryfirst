
class Package < ApplicationRecord
  validates_uniqueness_of :name, presence: true
  validates :name, presence: true
  validates :value, presence: true
  belongs_to :unit
end
