class User < ApplicationRecord
  has_and_belongs_to_many :roles, join_table: :user_roles
  validates_uniqueness_of :email
  has_secure_password
  has_many :books
end
