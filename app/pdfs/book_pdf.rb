class BookPdf < Prawn::Document
    def initialize(books)
        super(top_margin: 70)
        @books = books
        text "Book Items"
        text "Downloaded at "+Time.now.strftime("%b %-d, %Y")
        line_items
    end

    def line_items
        move_down 20
        table line_item_rows
    end

    def line_item_rows
        [["Name", "Cover Book Link"]]+
        @books.map do |item|
            [item.name.to_s, item.cover_book.to_s]
        end
    end
end