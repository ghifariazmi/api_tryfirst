class BookUploader < CarrierWave::Uploader::Base
    include Cloudinary::CarrierWave

    def extension_allowlist
        %w(jpg jpeg png)
    end

    def public_id
        return model.name
    end

    private

    def secure_filename
        hex = SecureRandom.uuid.split("-")
        hex.join("")
    end
end