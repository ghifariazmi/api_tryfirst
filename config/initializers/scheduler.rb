require 'rufus-scheduler'

scheduler = Rufus::Scheduler.singleton

scheduler.every '60m' do
    arrName = ['gilang','dirga','andesta','andesti']     
    name = arrName.shuffle.first
    user = User.find_by_id(1)
    user.name = name
    user.save
end  