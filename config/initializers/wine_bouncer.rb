# frozen_string_literal: true

WineBouncer.configure do |config|
  config.auth_strategy = :default

  # config.define_resource_owner do
  #   Agent.find_by_access_token(doorkeeper_access_token) if doorkeeper_access_token
  # end
end
