# frozen_string_literal: true

WineBouncer::OAuth2.class_eval do
  def doorkeeper_authorize!(*scopes)
    scopes = Doorkeeper.configuration.default_scopes if scopes.empty?
    valid_doorkeeper_token?(*scopes)
  end

  def valid_doorkeeper_token?(*scopes)
    @access_token = request.headers["Authorization"].try("split", " ").try(:last).try(:strip) || request.parameters["access_token"]
    access = Doorkeeper::AccessToken.find_by(token: @access_token)
    user   = User.find(access.resource_owner_id) if  access.present? && access.accessible?
    if access.present? && access.accessible? && user.present?
      the_scopes       = Doorkeeper::OAuth::Scopes.from_array(user.roles.try(:first).try(:code))
      no_scope_defined = scopes.blank? || scopes.any? { |s| the_scopes.exists?(s.to_s) }
      unless no_scope_defined
        raise WineBouncer::Errors::OAuthForbiddenError, name: WineBouncer::Errors::OAuthForbiddenError.class.to_s, state: :forbidden, description: scopes.join(", ")
      end
    else
      raise WineBouncer::Errors::OAuthUnauthorizedError, name: WineBouncer::Errors::OAuthUnauthorizedError.class.to_s, state: :unauthorized, description: "Access token unauthorized"
    end
    @current_user = user
  end


  def before
    return if WineBouncer.configuration.disable_block.call

    set_auth_strategy(WineBouncer.configuration.auth_strategy)
    auth_strategy.api_context = context
    context.extend(WineBouncer::AuthMethods)
    context.protected_endpoint = endpoint_protected?
    return unless context.protected_endpoint?
    self.doorkeeper_request = env
    doorkeeper_authorize!(*auth_scopes)
    context.doorkeeper_access_token = @access_token
    context.remote_resource_owner = @current_user
  end
end


WineBouncer::AuthMethods.class_eval do
  def resource_owner
    @remote_resource_owner
  end

  attr_writer :remote_resource_owner

  def client_credential_token?
    has_doorkeeper_token? && doorkeeper_access_token.resource_owner_id.nil?
  end

  def has_resource_owner?
    has_doorkeeper_token? && !!doorkeeper_access_token
  end
end
