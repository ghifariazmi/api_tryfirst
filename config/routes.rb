Rails.application.routes.draw do
  mount Admin::Api::Init, at: "/admin/"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get 'book', to: 'book#index'
  get 'book/borrow', to: 'book#create'
  post 'book/borrow', to: 'book#store'
  get 'book/pdf', to: 'book#exportpdf'
end
