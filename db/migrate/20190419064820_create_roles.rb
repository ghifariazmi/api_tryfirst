# frozen_string_literal: true

class CreateRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :roles do |t|
      t.string    :code
      t.string    :name
      t.timestamps
    end

    create_table(:agents_roles, id: false) do |t|
      t.references :agent
      t.references :role
    end
  end
end
