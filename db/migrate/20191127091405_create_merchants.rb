# frozen_string_literal: true

class CreateMerchants < ActiveRecord::Migration[5.2]
  def change
    create_table :merchants do |t|
      t.string :name
      t.string :merchant_app
      t.string :merchant_key
      t.string :base_url
      t.string :prefix_trx_id
      t.timestamps
    end
  end
end
