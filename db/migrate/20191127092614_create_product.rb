# frozen_string_literal: true

class CreateProduct < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.json    :image
      t.string  :code
      t.string  :name
      t.references :product_categories, index: true, foreign_key: { on_delete: :cascade }
      t.float   :het
      t.float   :hpp
      t.float   :hp
      t.references :merchants, index: true, foreign_key: { on_delete: :cascade }
      t.timestamps
    end
  end
end
