# frozen_string_literal: true

class CreateIngredients < ActiveRecord::Migration[5.2]
  def change
    create_table :ingredients do |t|
      t.string :name
      t.references :units, index: true, foreign_key: { on_delete: :cascade }
      t.float :stock
      t.timestamps
    end
  end
end
