# frozen_string_literal: true

class CreateUnitConversions < ActiveRecord::Migration[5.2]
  def change
    create_table :unit_conversions do |t|
      t.references  :ingredients, index: true, foreign_key: { on_delete: :cascade }
      t.references  :units, index: true, foreign_key: { on_delete: :cascade }
      t.float :value
      t.timestamps
    end
  end
end
