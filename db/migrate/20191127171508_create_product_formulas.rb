# frozen_string_literal: true

class CreateProductFormulas < ActiveRecord::Migration[5.2]
  def change
    create_table :product_formulas do |t|
      t.references  :product, index: true, foreign_key: { on_delete: :cascade }
      t.references  :ingredients, index: true, foreign_key: { on_delete: :cascade }
      t.float :value
      t.timestamps
    end
  end
end
