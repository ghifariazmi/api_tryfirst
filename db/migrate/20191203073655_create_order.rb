# frozen_string_literal: true

class CreateOrder < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer  :trx_no
      t.datetime :trx_date
      t.references :customer
      t.float :sub_total
      t.float :disc_persentage
      t.float :disc_value
      t.float :tax_persentage
      t.float :tax_value
      t.float :total
      t.references :merchant
      t.timestamps
    end
  end
end
