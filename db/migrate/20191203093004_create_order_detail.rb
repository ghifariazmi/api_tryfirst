# frozen_string_literal: true

class CreateOrderDetail < ActiveRecord::Migration[5.2]
  def change
    create_table :order_details do |t|
      t.references  :order
      t.references  :product
      t.float :qty
      t.references :unit
      t.float :price
      t.float :total
      t.timestamps
    end
  end
end
