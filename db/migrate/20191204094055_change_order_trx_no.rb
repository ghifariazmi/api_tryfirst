# frozen_string_literal: true

class ChangeOrderTrxNo < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :trx_no, :string
  end
end
