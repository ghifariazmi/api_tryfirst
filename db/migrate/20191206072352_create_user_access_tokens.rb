# frozen_string_literal: true

class CreateUserAccessTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :user_access_tokens do |t|
      t.references :user
      t.string :token
      t.string :refresh_token
      t.datetime :revoked_at
      t.datetime :expired_at
      t.timestamps
    end
  end
end
