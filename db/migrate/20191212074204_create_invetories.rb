class CreateInvetories < ActiveRecord::Migration[5.2]
  def change
    create_table :inventories do |t|
      t.string :name
      t.references :unit
      t.float :current
      t.float :stock_archive
      t.timestamps
    end
  end
end
