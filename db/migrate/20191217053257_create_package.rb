class CreatePackage < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :name
      t.references :unit
      t.float :value
      t.timestamps
    end
  end
end
