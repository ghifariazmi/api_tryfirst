class AddProcudtIdtoIngredients < ActiveRecord::Migration[5.2]
  def change
    # change_table :ingredients do |t|
    #   t.references :product
    #   t.references :inventory
    #   t.rename :stock, :value
    #   t.remove :units_id
    # end

    reversible do |dir|
      change_table :ingredients do |t|
        dir.up do
          t.references :product
          t.references :inventory
          t.rename :stock, :value
          t.remove :units_id

        end
        dir.down do
          t.remove :product
          t.remove :inventory
          t.rename :value, :stock
          t.integer :units_id
        end
      end
    end
  end
end
