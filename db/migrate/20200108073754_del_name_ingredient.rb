class DelNameIngredient < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      change_table :ingredients do |t|
        dir.up do
          t.remove :name

        end
        dir.down do
          t.string :name
        end
      end
    end
  end
end
