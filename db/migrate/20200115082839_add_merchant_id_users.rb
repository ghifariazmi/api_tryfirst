class AddMerchantIdUsers < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      change_table :users do |t|
        dir.up do
          t.references :merchant
        end
        dir .down do
          t.remove :merchant
        end
      end
    end
  end
end
