class AddEnableProduct < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      change_table :products do |t|
        dir.up do
          t.boolean :enable, default: true
        end
        dir.down do
          t.remove :enable
        end
      end
    end
  end
end
