class AddStatusOrder < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      change_table :orders do |t|
        dir.up do
         t.integer :status
        end
        dir.down do
          t.remove :status
        end
      end
    end
  end
end
