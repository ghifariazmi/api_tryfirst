class AddNoteOrderDetail < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      change_table :order_details do |t|
        dir.up do
          t.string :note
        end
        dir.down do
          t.remove :note
        end
      end
    end
  end
end
