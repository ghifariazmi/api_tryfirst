# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_05_25_035440) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agents", force: :cascade do |t|
    t.string "privy_id"
    t.string "access_token"
    t.string "name"
    t.boolean "enable", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["access_token"], name: "index_agents_on_access_token"
    t.index ["privy_id"], name: "index_agents_on_privy_id"
  end

  create_table "agents_roles", id: false, force: :cascade do |t|
    t.bigint "agent_id"
    t.bigint "role_id"
    t.index ["agent_id"], name: "index_agents_roles_on_agent_id"
    t.index ["role_id"], name: "index_agents_roles_on_role_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "name"
    t.string "cover_book"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_books_on_user_id"
  end

  create_table "configurations", id: false, force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.index ["key"], name: "index_configurations_on_key"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.float "discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "ingredients", force: :cascade do |t|
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_id"
    t.bigint "inventory_id"
    t.index ["inventory_id"], name: "index_ingredients_on_inventory_id"
    t.index ["product_id"], name: "index_ingredients_on_product_id"
  end

  create_table "inventories", force: :cascade do |t|
    t.string "name"
    t.bigint "unit_id"
    t.float "current"
    t.float "stock_archive"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["unit_id"], name: "index_inventories_on_unit_id"
  end

  create_table "merchants", force: :cascade do |t|
    t.string "name"
    t.string "merchant_app"
    t.string "merchant_key"
    t.string "base_url"
    t.string "prefix_trx_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.bigint "resource_owner_id", null: false
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes"
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["resource_owner_id"], name: "index_oauth_access_grants_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.bigint "resource_owner_id"
    t.bigint "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.boolean "confidential", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "order_details", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "product_id"
    t.float "qty"
    t.bigint "unit_id"
    t.float "price"
    t.float "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "note"
    t.index ["order_id"], name: "index_order_details_on_order_id"
    t.index ["product_id"], name: "index_order_details_on_product_id"
    t.index ["unit_id"], name: "index_order_details_on_unit_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "trx_no"
    t.datetime "trx_date"
    t.bigint "customer_id"
    t.float "sub_total"
    t.float "disc_persentage"
    t.float "disc_value"
    t.float "tax_persentage"
    t.float "tax_value"
    t.float "total"
    t.bigint "merchant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.index ["customer_id"], name: "index_orders_on_customer_id"
    t.index ["merchant_id"], name: "index_orders_on_merchant_id"
  end

  create_table "packages", force: :cascade do |t|
    t.string "name"
    t.bigint "unit_id"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["unit_id"], name: "index_packages_on_unit_id"
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_formulas", force: :cascade do |t|
    t.bigint "product_id"
    t.bigint "ingredients_id"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ingredients_id"], name: "index_product_formulas_on_ingredients_id"
    t.index ["product_id"], name: "index_product_formulas_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.json "image"
    t.string "code"
    t.string "name"
    t.bigint "product_categories_id"
    t.float "het"
    t.float "hpp"
    t.float "hp"
    t.bigint "merchants_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "enable", default: true
    t.index ["merchants_id"], name: "index_products_on_merchants_id"
    t.index ["product_categories_id"], name: "index_products_on_product_categories_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "unit_conversions", force: :cascade do |t|
    t.bigint "ingredients_id"
    t.bigint "units_id"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ingredients_id"], name: "index_unit_conversions_on_ingredients_id"
    t.index ["units_id"], name: "index_unit_conversions_on_units_id"
  end

  create_table "units", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_access_tokens", force: :cascade do |t|
    t.bigint "user_id"
    t.string "token"
    t.string "refresh_token"
    t.datetime "revoked_at"
    t.datetime "expired_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_access_tokens_on_user_id"
  end

  create_table "user_roles", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_user_roles_on_role_id"
    t.index ["user_id"], name: "index_user_roles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "password_digest"
    t.string "email"
    t.string "phone"
    t.boolean "enable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "merchant_id"
    t.index ["merchant_id"], name: "index_users_on_merchant_id"
  end

  add_foreign_key "books", "users"
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "product_formulas", "ingredients", column: "ingredients_id", on_delete: :cascade
  add_foreign_key "product_formulas", "products", on_delete: :cascade
  add_foreign_key "products", "merchants", column: "merchants_id", on_delete: :cascade
  add_foreign_key "products", "product_categories", column: "product_categories_id", on_delete: :cascade
  add_foreign_key "unit_conversions", "ingredients", column: "ingredients_id", on_delete: :cascade
  add_foreign_key "unit_conversions", "units", column: "units_id", on_delete: :cascade
end
