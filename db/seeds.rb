adminSuper = Role.find_or_create_by(code: "super_admin", name: "Super Admin")
Role.find_or_create_by(code: "kasir", name: "Kasir")
admin = User.new(email: "admin@admin.com", password: "password", password_confirmation: "password", name: "Admin", phone: "080989999", enable: true)
admin.roles << adminSuper
admin.save

Doorkeeper::Application.create! name: "Pos Mage App", redirect_uri: "https://localhost:3000"
Doorkeeper::Application.create! name: "Pos Mage Dashboard", redirect_uri: "https://localhost:3000"
